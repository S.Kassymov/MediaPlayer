package com.example.mediaplayer



import android.app.NotificationManager
import android.app.TimePickerDialog
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.method.ScrollingMovementMethod
import android.view.View
import android.widget.SeekBar
import android.widget.TimePicker
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import kotlinx.android.synthetic.main.activity_main.*
import java.io.InputStream
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity() {

    lateinit var runnable: Runnable
    private var handler = Handler()
    var currentIndex = 0
    var totalTime =""
    lateinit var timePicker: TimePicker
    var time :Long = 0


    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val songs: ArrayList<Int> = ArrayList()
        songs.add(0, R.raw.gorillaz_humility)
        songs.add(1, R.raw.nirvana_smells_like_teen_spirit)
        songs.add(2, R.raw.devil_trigger)
        songs.add(3, R.raw.the_weeknd_blinding_lights)
        songs.add(4, R.raw.whitney_houston_i_wanna_dance_with_somebody)

        var mediaPlayer: MediaPlayer = MediaPlayer.create(this, songs[currentIndex])
        songNames()
        totalTime =  createTimeLabel(mediaPlayer.duration)
        remainingTimeLabel.text = totalTime
        seekbar.progress = 0
        seekbar.max = mediaPlayer.duration



        music_lyrics.movementMethod = ScrollingMovementMethod()

        playBtn.setOnClickListener{
            if(!mediaPlayer.isPlaying){
                mediaPlayer.start()
                playBtn.setBackgroundResource(R.drawable.pause_24)
            }else{
                mediaPlayer.pause()
                playBtn.setBackgroundResource(R.drawable.play24)
            }
        }
        previousBtn.setOnClickListener(View.OnClickListener {
            if (mediaPlayer!= null) {
                playBtn.setBackgroundResource(R.drawable.pause_24)
            }
            if (currentIndex < songs.size - 1) {
                currentIndex++
            } else {
                currentIndex = 0
            }
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.stop()
            }
            mediaPlayer = MediaPlayer.create(applicationContext, songs[currentIndex])
            totalTime = createTimeLabel(mediaPlayer.duration)
            remainingTimeLabel.text =  createTimeLabel(mediaPlayer.duration)
            seekbar.max = mediaPlayer.duration
            mediaPlayer.start()
            songNames()
        })
        nextBtn.setOnClickListener(View.OnClickListener {
            if (mediaPlayer != null) {
                playBtn.setBackgroundResource(R.drawable.pause_24)
            }
            if (currentIndex > 0) {
                currentIndex--
            } else {
                currentIndex = songs.size - 1
            }
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.stop()
            }
            mediaPlayer = MediaPlayer.create(applicationContext, songs[currentIndex])
            totalTime = createTimeLabel(mediaPlayer.duration)
            remainingTimeLabel.text =  createTimeLabel(mediaPlayer.duration)
            seekbar.max = mediaPlayer.duration
            mediaPlayer.start()
            songNames()
        })

        timer.setOnClickListener {
            val cal = Calendar.getInstance()
            val timeListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
                cal.set(Calendar.HOUR_OF_DAY, hour)
                cal.set(Calendar.MINUTE, minute)
                var hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY)
                var minutes = Calendar.getInstance().get(Calendar.MINUTE)
                var current =  hour*3600 + minute*60
                time = current - cal.timeInMillis

                Handler(Looper.getMainLooper()).postDelayed({
                    val builder = NotificationCompat.Builder(this)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentTitle("Music")
                            .setContentText("Stop playing music")
                    val notification = builder.build()

                    val notificationManager =
                            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                    notificationManager.notify(1, notification)

                }, time)

            }

            TimePickerDialog(this,timeListener,cal.get(Calendar.HOUR_OF_DAY),cal.get(Calendar.MINUTE),true).show()
        }

        lyrics.setOnClickListener{
            if(music_lyrics.visibility== View.GONE){
                music_lyrics.visibility = View.VISIBLE
                image.visibility = View.GONE
            }else{
                music_lyrics.visibility = View.GONE
                image.visibility = View.VISIBLE
            }
        }



        seekbar.setOnSeekBarChangeListener(object: SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(p0: SeekBar?, pos:Int, changed: Boolean){
                if(changed){
                    mediaPlayer.seekTo(pos)
                }
            }
            override fun onStartTrackingTouch(p0: SeekBar?){

            }
            override fun onStopTrackingTouch(p0: SeekBar?){

            }
        })

        runnable = Runnable {
            seekbar.progress = mediaPlayer.currentPosition
            handler.postDelayed(runnable,1000)

            var elapsedTime = createTimeLabel(mediaPlayer.currentPosition)

            if(elapsedTime == totalTime){
                    if (mediaPlayer != null) {
                        playBtn.setBackgroundResource(R.drawable.pause_24)
                    }
                    if (currentIndex > 0) {
                        currentIndex--
                    } else {
                        currentIndex = songs.size - 1
                    }
                    if (mediaPlayer.isPlaying()) {
                        mediaPlayer.stop()
                    }
                    mediaPlayer = MediaPlayer.create(applicationContext, songs[currentIndex])
                    totalTime = createTimeLabel(mediaPlayer.duration)
                    remainingTimeLabel.text =  createTimeLabel(mediaPlayer.duration)
                    seekbar.max = mediaPlayer.duration
                    mediaPlayer.start()
                songNames()
            }
            elapsedTimeLabel.text = elapsedTime

        }
        handler.postDelayed(runnable,1000)

        mediaPlayer.setOnCompletionListener {
            playBtn.setBackgroundResource(R.drawable.play24)
            seekbar.progress = 0
        }
    }

    fun createTimeLabel(time: Int): String {
        var timeLabel = ""
        var min = time / 1000 / 60
        var sec = time / 1000 % 60

        timeLabel = "$min:"
        if (sec < 10) timeLabel += "0"
        timeLabel += sec

        return timeLabel
    }

    fun readLyrics(name:String){
        val file: String = applicationContext.assets.open(name).bufferedReader().use{
            it.readText()
        }
        music_lyrics.text = file
    }

    fun setImage(name:String){
        var inputStream: InputStream = assets.open(name)

        var b = BitmapFactory.decodeStream(inputStream)
        b.density = Bitmap.DENSITY_NONE
        val d: Drawable = BitmapDrawable(b)
        image.background = d

    }




    private fun songNames() {
        if (currentIndex == 0) {
            music_name!!.text = "Humility"
            musician_name!!.text = "Gorillaz"
            setImage("gorillaz.jpg")
            readLyrics("humility.txt")

        }
        if (currentIndex == 1) {
            music_name!!.text = "Smells like teen spirit"
            musician_name!!.text = "Nirvana"
            setImage("nirvana.jpg")
            readLyrics("teenspirit.txt")
        }
        if (currentIndex == 2) {
            music_name!!.text = "Devil Trigger"
            musician_name!!.text = "Casey Edwards & Ali Edwardss"
            setImage("devil.jpg")
            readLyrics("devil.txt")
        }
        if (currentIndex == 3) {
            music_name!!.text = "Blinding lights"
            musician_name!!.text = "The weeknd"
            setImage("Blinding_Lights.jpg")
            readLyrics("weeknd.txt")
        }
        if (currentIndex == 4) {
            music_name!!.text = "I wanna dance with somebody"
            musician_name!!.text = "Whitney Houston"
            setImage("whitney.jpg")
            readLyrics("dance.txt")
        }
    }

}

